/*
 * copyright (c) 2019 Joas Weeda
 * All rights reserved
 */

package nl.bioinf.ojweeda;

import weka.classifiers.lazy.IBk;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.CSVLoader;
import weka.core.converters.ConverterUtils.DataSource;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Add;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

/**
 * If you saved a model to a file in WEKA, you can use it reading the generated java object.
 * Here is an example with Random Forest classifier (previously saved to a file in WEKA):
 * import java.io.ObjectInputStream;
 * import weka.core.Instance;
 * import weka.core.Instances;
 * import weka.core.Attribute;
 * import weka.core.FastVector;
 * import weka.classifiers.trees.RandomForest;
 * RandomForest rf = (RandomForest) (new ObjectInputStream(PATH_TO_MODEL_FILE)).readObject();
 * <p>
 * or
 * RandomTree treeClassifier = (RandomTree) SerializationHelper.read(new FileInputStream("model.weka")));
 */

public class WekaRunner {

    void start(String givenFile) {
        String expected = "SW, W, T, R, P, SO";

        try {
            IBk fromFile = loadClassifier();

            CSVLoader csvLoader = new CSVLoader();
            csvLoader.setSource(new File(givenFile));

            csvLoader.setNominalAttributes("first-last");
            Instances unknownInstancesFromCsv = csvLoader.getDataSet();

            if (unknownInstancesFromCsv.classIndex() == -1)
                unknownInstancesFromCsv.setClassIndex(unknownInstancesFromCsv.numAttributes() - 1);

            Instances newData = new Instances(unknownInstancesFromCsv);
            // 1. nominal attribute
            Add filter = new Add();
            filter.setAttributeIndex("last");
            filter.setNominalLabels("Sw, W, T, R, P, SO");
            filter.setAttributeName("groups");
            filter.setInputFormat(newData);
            newData = Filter.useFilter(newData, filter);

            newData.setClassIndex(newData.numAttributes() - 1);
            System.out.println("newData # attributes = " + newData.numAttributes());
            System.out.println("newData class index= " + newData.classIndex());

            //System.out.println("unknownInstancesFromCsv = " + newData);
            //System.exit(0);

//            System.out.println("\nunclassified unknownInstances = \n" + unknownInstances);
            classifyNewInstance(fromFile, newData);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void classifyNewInstance(IBk tree, Instances unknownInstances) throws Exception {
        // create copy
        Instances labeled = new Instances(unknownInstances);
        // label instances
        for (int i = 0; i < unknownInstances.numInstances(); i++) {
            double clsLabel = tree.classifyInstance(unknownInstances.instance(i));
            labeled.instance(i).setClassValue(clsLabel);
        }
        System.out.println("\nNew, labeled = \n" + labeled);
    }

    private IBk loadClassifier() throws Exception {
        // deserialize model
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File("../IBK.model");
        return (IBk) weka.core.SerializationHelper.read(file.getAbsolutePath());
    }


//    private void printInstances(Instances instances) {
//        int numAttributes = instances.numAttributes();
//
//        for (int i = 0; i < numAttributes; i++) {
//            System.out.println("attribute " + i + " = " + instances.attribute(i));
//        }
//
//        System.out.println("class index = " + instances.classIndex());
////        Enumeration<Instance> instanceEnumeration = instances.enumerateInstances();
////        while (instanceEnumeration.hasMoreElements()) {
////            Instance instance = instanceEnumeration.nextElement();
////            System.out.println("instance " + instance. + " = " + instance);
////        }
//
//        //or
//        int numInstances = instances.numInstances();
//        for (int i = 0; i < numInstances; i++) {
//            if (i == 5) break;
//            Instance instance = instances.instance(i);
//            //System.out.println("instance = " + instance);
//        }
//    }

    //    public static void main(final String[] args) {
//        try {
//            ApacheCLIOptionsProvider op = new ApacheCLIOptionsProvider(args);
//            if (op.helpRequested()) {
//                op.printHelp();
//                return;
//            }
//            WekaRunner runner = new WekaRunner();
//            runner.start(op.getFileName());
//            MessagingController controller = new MessagingController(op);
//            controller.start();
//        } catch (IllegalStateException se) {
//            System.err.println("Something went wrong processing the commandline! " + Arrays.toString(args));
//            System.err.println("Failed because: " + se.getMessage());
//        }
//    }
}
