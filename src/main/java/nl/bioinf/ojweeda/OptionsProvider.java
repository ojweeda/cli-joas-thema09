/*
 * copyright (c) 2019 Joas Weeda
 * All rights reserved
 */

package nl.bioinf.ojweeda;

/**
 * interface that specifies which options should be provided to the tool.
 * @author michiel
 */
public interface OptionsProvider {
    /**
     * serves the age of the application user.
     * @return userAge the user age
     */
    String getFileName();
}