/*
* copyright (c) 2019 Joas Weeda
* All rights reserved
*/

package nl.bioinf.ojweeda;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import java.io.*;
import java.io.FileWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
/**
 * This class implements OptionsProvider interface by parsing the passed command line arguments.
 *
 * @author ojweeda
 */
public class ApacheCLIOptionsProvider implements OptionsProvider {
    private static final String FILE = "file";
    private static final String HELP = "help";
    private static final String ATTR = "attr";

    private String[] clArguments;
    private Options options;
    private CommandLine commandLine;
    private String input;


    /**
     * constructs with the command line array.
     *
     * @param args the CL array
     */
    public ApacheCLIOptionsProvider(final String[] args) {
        this.clArguments = args;
        initialize();
    }

    /**
     * Options initialization and processing.
     */
    private void initialize() {
        buildOptions();
        processCommandLine();
    }

    /**
     * check if help was requested; if so, return true.
     * @return helpRequested
     */
    public boolean helpRequested() {
        return this.commandLine.hasOption(HELP);
    }

    /**
     * builds the Options object.
     */
    private void buildOptions() {
        // create Options object
        this.options = new Options();
        Option inputFile = new Option("f", FILE, true, "input csv file");
        Option SingleAttr = new Option("l", ATTR, true, "Single line of attributes, must be log2 transformed");
        Option Help = new Option("h", HELP, false, "Prints this help message");

        options.addOption(inputFile);
        options.addOption(SingleAttr);
        options.addOption(Help);
    }


    /**
     * processes the command line arguments.
     */
    private void processCommandLine() {
        try {
            WekaRunner wekaIBK = new WekaRunner();

            CommandLineParser parser = new DefaultParser();
            this.commandLine = parser.parse(this.options, this.clArguments);

            if (commandLine.hasOption(FILE)) {
                String input = commandLine.getOptionValue(FILE);
                if (extensionCheck(input)) {
                    wekaIBK.start(input);
                }
            } else if (commandLine.hasOption(ATTR)) {
                input = commandLine.getOptionValue(ATTR);

//                String data_sample = writeToFile(input);
//                wekaIBK.start(data_sample);

            } else if (commandLine.hasOption(HELP)) {
                printHelp();
            } else {
                printHelp();
            }
        } catch (ParseException ex) {
            throw new IllegalArgumentException(ex);
        }
    }

    //                    System.out.println("File approved");
//                } else {
//                    throw new IllegalArgumentException("Filename is not approved, file extension is wrong or something idk.");
//                }
//            } else {
//                System.out.println("Filename not received, please add filename to command");
//            }

    //            String agestr = this.commandLine.getOptionValue(AGE, "42");
//            this.age = Integer.parseInt(agestr);
     /*
    Fetches the file extension of the given file
     */
    private String getFileExtension(File file) {
        String name = file.getName();
        int lastIndexOf = name.lastIndexOf(".");
        if (lastIndexOf == -1) {
            return ""; // empty extension
        }
        return name.substring(lastIndexOf);
    }

    private boolean extensionCheck(String datafile) {
        String fileExtension;
        File file = new File(datafile);
        fileExtension = getFileExtension(file);
        boolean isValidExtension = false;
        // Checks if fileExtension is a .csv type
        if (fileExtension.equals(".csv")) {
            isValidExtension = true;
        }
        else{
            System.err.println("Please input a .csv file");
        }
        return isValidExtension;
    }

//        private static String writeToFile(String args){
//            String fileName = "../../data/singleInstance.csv";
//
//            try{
//                CSVWriter writer = new CSVWriter(new FileWriter(fileName));
//
//                // Create the attributes
//                String[] attr = {"huml", "humw", "ulnal", "ulnaw", "feml", "femw",
//                        "tibl", "tibw", "tarl", "tarw"};
//
//                String[] instance = args.split(",");
//
//                // Adding the new instances and attributes to the file
//                List<String[]> list = new ArrayList<>();
//                list.add(attr);
//                list.add(instance);
//
//                writer.writeAll(list);
//                writer.flush();
//            }
//            catch (IOException e){
//                System.err.println("Data could not be parsed");
//            }
//
//            return fileName;
//        }

    /**
     * prints help.
     */
    public void printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("MyTool", options);
    }

    @Override
    public String getFileName() {
        return this.commandLine.getOptionValue(FILE, "data/birds_clean_without_sample.csv");
    }

}
